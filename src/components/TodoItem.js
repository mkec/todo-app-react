import React from 'react'

class TodoItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editText: ''
        };
    }

    handleEdit() {
        this.setState({editText: this.props.todo.text})
        this.props.handleEdit(this.props.todo.key)
    }

    onChange = (event) => {
        this.setState({editText: event.target.value})
    }

    onKeyDown = (event) => {
        if (event.key === "Escape") {
            this.setState({editText: this.props.todo.text})
            this.props.onCancel()
        } else if (event.key === "Enter") {
            this.onSubmit()
        }
    }

    onSubmit = () => {
        if (this.state.editText !== "") {
            this.props.saveEdit(this.props.todo.key, this.state.editText)
        } else {
            this.props.onCancel()
            this.props.removeTodo(this.props.todo.key)
        }
    }

    render() {
        const todo = this.props.todo

        return (
            <li className={(todo.completed ? 'completed' : '') + (this.props.editing ? ' editing' : '')}>
                <div className="view">
                    <input
                        className="toggle"
                        type="checkbox"
                        checked={todo.completed}
                        onChange={() => this.props.markTodoDone(this.props.todo.key)}
                    />
                    <label onDoubleClick={() => this.handleEdit()}>
                        {todo.text}
                    </label>
                    <button className="destroy" onClick={() => this.props.removeTodo(todo.key)}/>
                </div>
                <input
                    className="edit"
                    value={this.state.editText}
                    onChange={this.onChange}
                    onKeyDown={this.onKeyDown}
                    onBlur={this.onSubmit}
                />
            </li>
        )
    }
}

export default TodoItem