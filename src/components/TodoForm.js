import React from 'react'

const TodoForm = (props) => (
    <header className="header">
        <input
            className="new-todo"
            placeholder="What needs to be done?"
            value={props.value}
            onChange={props.onChange}
            onKeyDown={props.onKeyDown}
            autoFocus={true}
        />
    </header>
);

export default TodoForm