import React, {Component} from 'react'
import TodoItem from "./TodoItem";

class TodoList extends Component {
    render() {

        if (this.props.hidden) return null

        const todoItems = this.props.todoItems.map(function (todo) {
            return (
                <TodoItem
                    key={todo.key}
                    todo={todo}
                    editing={this.props.editing === todo.key}
                    removeTodo={this.props.removeTodo}
                    markTodoDone={this.props.markTodoDone}
                    handleEdit={this.props.handleEdit}
                    onKeyDown={this.props.onKeyDown}
                    saveEdit={this.props.saveEdit}
                    onCancel={this.props.onCancel}
                />
            );
        }, this);

        return (
            <section className="main">
                <input
                    id="toggle-all"
                    className="toggle-all"
                    type="checkbox"
                    checked={this.props.allCompleted}
                    onChange={this.props.markAllTodoDone}
                />
                <label
                    htmlFor="toggle-all"
                />
                <ul className="todo-list">
                    {todoItems}
                </ul>
            </section>
        )
    }
}

export default TodoList