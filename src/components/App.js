import React, {Component} from 'react'
import PropTypes from 'prop-types';
import TodoHeader from "./TodoHeader";
import TodoForm from "./TodoForm";
import TodoList from "./TodoList";
import TodoFooter from "./TodoFooter";
import {HashRouter as Router, Route, Switch} from "react-router-dom";

const App = () => (
    <Router>
        <Switch>
            <Route exact path="/"><TodoApp showing={showingTodos.ALL}/></Route>
            <Route path="/active"><TodoApp showing={showingTodos.ACTIVE}/></Route>
            <Route path="/completed"><TodoApp showing={showingTodos.COMPLETED}/></Route>
        </Switch>
    </Router>
)

export const showingTodos = {
    ALL: 'all',
    ACTIVE: 'active',
    COMPLETED: 'completed'
}

App.propTypes = {
    showingTodos: PropTypes.oneOf(Object.keys(showingTodos))
}

class TodoApp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            todoItems: [],
            newTodo: '',
            editing: null
        };
    }

    addTodo = (newTodoItem) => {
        if (this.state.newTodo !== "") {
            const {todoItems} = this.state
            this.setState({todoItems: todoItems.concat(newTodoItem)})
        }
    }

    removeTodo = (key) => {
        const {todoItems} = this.state
        const newTodoItems = todoItems.filter(function (item) {
            return (item.key !== key)
        })
        this.setState({todoItems: newTodoItems})
    }

    clearCompleted = () => {
        const {todoItems} = this.state
        const newTodoItems = todoItems.filter(function (item) {
            return !item.completed;
        })
        this.setState({todoItems: newTodoItems})
    }

    markTodoDone = (key) => {
        const {todoItems} = this.state
        const newTodoItems = todoItems.map(function (todo) {
            todo.completed = (todo.key === key) ? !todo.completed : todo.completed
            return todo
        })
        this.setState({todoItems: newTodoItems});
    }

    markAllTodoDone = (event) => {
        let checked = event.target.checked;
        const {todoItems} = this.state
        const newTodoItems = todoItems.map(function (todo) {
            todo.completed = checked
            return todo
        })
        this.setState({todoItems: newTodoItems});
    }

    handleChange = (event) => {
        this.setState({newTodo: event.target.value})
    }

    handleKeyPress = (event) => {
        if (event.key === 'Enter') {
            this.addTodo({
                key: Date.now(),
                text: this.state.newTodo,
                completed: false
            })
            this.setState({newTodo: ''})
        }
    }

    handleEdit = (key) => {
        this.setState({editing: key})
    }

    saveEdit = (todoKey, newText) => {
        const {todoItems} = this.state
        const newTodoItems = todoItems.map(function (todo) {
            todo.text = (todo.key === todoKey) ? newText : todo.text
            return todo
        })
        this.setState({todoItems: newTodoItems});
        this.handleCancel()
    }

    handleCancel = () => {
        this.setState({editing: null})
    }

    componentDidMount() {
        const json = localStorage.getItem('todoItems')
        if (json) {
            const items = JSON.parse(json)
            this.setState({todoItems: items})
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevState.todoItems !== this.state.todoItems) {
            const json = JSON.stringify(this.state.todoItems)
            localStorage.setItem('todoItems', json)
        }
    }

    render() {
        const todos = this.state.todoItems;
        const showing = this.props.showing;

        const shawnTodos = todos.filter(function (todo) {
            switch (showing) {
                case showingTodos.ACTIVE:
                    return !todo.completed;
                case showingTodos.COMPLETED:
                    return todo.completed;
                default:
                    return true;
            }
        })

        const completedCount = todos.filter(function (todo) {
            return todo.completed;
        }).length
        const activeTodoCount = todos.length - completedCount

        return (
            <section className='todoapp'>
                <TodoHeader/>
                <TodoForm value={this.state.newTodo}
                          addTodo={this.addTodo}
                          onChange={this.handleChange}
                          onKeyDown={this.handleKeyPress}
                />
                <TodoList todoItems={shawnTodos}
                          allCompleted={activeTodoCount === 0}
                          editing={this.state.editing}
                          saveEdit={this.saveEdit}
                          removeTodo={this.removeTodo}
                          markTodoDone={this.markTodoDone}
                          markAllTodoDone={this.markAllTodoDone}
                          handleEdit={this.handleEdit}
                          handleChange={this.handleChange}
                          onKeyDown={this.handleKeyPress}
                          onCancel={this.handleCancel}
                          hidden={todos.length === 0}
                />
                <TodoFooter nowShowing={this.props.showing}
                            activeTodoCount={activeTodoCount}
                            completedCount={completedCount}
                            clearCompleted={this.clearCompleted}
                            hidden={todos.length === 0}
                />
            </section>
        )
    }
}

export default App