import React, {Component} from 'react'
import {Link} from "react-router-dom";
import {showingTodos} from "./App";

const TodoFooter = (props) => {

    if (props.hidden) return null

    return (
        <footer className="footer">
            <span className="todo-count">
                <strong>{props.count}</strong> {props.activeTodoCount} left
            </span>
            <ul className="filters">
                <li>
                    <Link
                        to="/"
                        className={props.nowShowing === showingTodos.ALL ? 'selected' : ''}>
                        All
                    </Link>
                </li>
                {' '}
                <li>
                    <Link
                        to="/active"
                        className={props.nowShowing === showingTodos.ACTIVE ? 'selected' : ''}>
                        Active
                    </Link>
                </li>
                {' '}
                <li>
                    <Link
                        to="/completed"
                        className={props.nowShowing === showingTodos.COMPLETED ? 'selected' : ''}>
                        Completed
                    </Link>
                </li>
            </ul>
            <ClearButton hidden={props.completedCount === 0}
                         onClick={props.clearCompleted}
            />
        </footer>
    )
}

const ClearButton = (props) => {
    if (props.hidden) return null
    return (
        <button
            className="clear-completed"
            onClick={props.onClick}>
            Clear completed
        </button>
    )
}

export default TodoFooter